# minecraft

## Current version

Look at git tag not Chart.yaml.

## Helmfile example:

```
# DOMAIN=skyblock.aztek.io helmfile -f helmfile.yaml diff

repositories:
- name: aztek
  url: https://charts.aztek.io

releases:
- name: {{ requiredEnv "DOMAIN" | replace "." "-" }}
  namespace: minecraft
  chart: aztek/minecraft
  version: v0.1.0
  # scale deployment to 0 for rwo pvc
  hooks:
  - events: ["presync"]
    showlogs: true
    command: "kubectl"
    args: ['scale', 'deployment', '-n', 'minecraft', '{{ requiredEnv "DOMAIN" | replace "." "-" }}-minecraft', '--replicas=0']
  values:
  - service:
      annotations:
        external-dns.alpha.kubernetes.io/hostname: {{ requiredEnv "DOMAIN"  }}
    minecraft:
      env:
        MINECRAFT_EULA: true
        MINECRAFT_OPS: WastedSuper,JollyGiant
  wait: true
  timeout: 300
```
